package servlets.api.users;

import servlets.pojo.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/myInfo")
public class MyInfo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.getWriter().println("This servlet contains your session information.");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userInfo");
        if(user == null){
            response.getWriter().println("Please enter any info about yourself first");
            return;
        }
        response.getWriter().println(session.getAttribute("userInfo"));
    }
}
