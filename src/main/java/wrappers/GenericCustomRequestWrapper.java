package wrappers;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class GenericCustomRequestWrapper extends HttpServletRequestWrapper {

    public GenericCustomRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    public String getParameter(String name) {
        System.out.println("getParameter called from GenericCustomRequestWrapper");
        String value = super.getParameter(name);
        if (value != null && !value.isEmpty()){
            return value;
        }
        return null;
    }

}
