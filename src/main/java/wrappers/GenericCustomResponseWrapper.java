package wrappers;

import javax.servlet.ServletResponse;
import javax.servlet.ServletResponseWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class GenericCustomResponseWrapper extends HttpServletResponseWrapper {

    public GenericCustomResponseWrapper(HttpServletResponse response) {
        super(response);
    }

    public void setHeader(String name, String value) {
        if (name != null && !name.isEmpty() && value != null && !value.isEmpty()) {
            super.setHeader(name, value);
        }
    }
}
