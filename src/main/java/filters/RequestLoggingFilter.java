package filters;

import wrappers.GenericCustomRequestWrapper;
import wrappers.GenericCustomResponseWrapper;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@WebFilter(filterName = "RequestLoggingFilter")
public class RequestLoggingFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        Cookie[] cookies = req.getCookies();
        if(cookies == null){
            System.out.println(req.getRemoteAddr() + " | No cookies found");
            chain.doFilter(request, response);
            return;
        }
        Arrays.stream(cookies).filter(
                cookie -> cookie.getName().equals("user")
        ).forEach(cookie -> System.out.println(req.getRemoteAddr() + " | Client side Cookie:{"+cookie.getName()+","+cookie.getValue()+"}"));
        chain.doFilter(req, response);
    }
}
