package filters;

import wrappers.GenericCustomRequestWrapper;
import wrappers.GenericCustomResponseWrapper;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "WrapperImpFilter")
public class WrapperImpFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        // request wrapper is used to check if parameters exist and not empty inside requested servlet itself (optional)
        GenericCustomRequestWrapper requestWrapper = new GenericCustomRequestWrapper(req);
        GenericCustomResponseWrapper responseWrapper = new GenericCustomResponseWrapper(res);
        responseWrapper.addHeader("CUSTOM-HEADER", "HELLO FROM CUSTOM RESPONSE WRAPPER");
        chain.doFilter(requestWrapper, responseWrapper);
    }
}
